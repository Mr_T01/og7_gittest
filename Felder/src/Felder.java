import java.util.Arrays;

/**
 *
 * Übungsklasse zu Feldern
 *
 * @version 1.1 vom 21.09.2018
 * @author Tom Schrogl
 */

public class Felder {

	// unsere Zahlenliste zum Ausprobieren
	private int[] zahlenliste = { 5, 8, 4, 3, 9, 1, 2, 7, 6, 0 };

	// Konstruktor
	public Felder() {
	}

	// Methode die Sie implementieren sollen
	// ersetzen Sie den Befehl return 0 durch return ihre_Variable

	// die Methode soll die größte Zahl der Liste zurückgeben
	public int maxElement() {
		int zahl = zahlenliste[0];
		for (int i = 0; i<zahlenliste.length; i++) {
			if ( zahl < zahlenliste[i])
				zahl = zahlenliste[i];	
		}
		
		return zahl;
	}

	// die Methode soll die kleinste Zahl der Liste zurückgeben
	public int minElement() {
		Arrays.sort(zahlenliste);
		int zahl = zahlenliste[0];
		return zahl;
	}

	// die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
	public int durchschnitt() {
		int durchschnitt = 0;
		for (int i = 0; i<zahlenliste.length; i++) {
			durchschnitt += zahlenliste[i];
		}
		durchschnitt = durchschnitt / zahlenliste.length;
		return durchschnitt;
	}

	// die Methode soll die Anzahl der Elemente zurückgeben
	// der Befehl zahlenliste.length; könnte hierbei hilfreich sein
	public int anzahlElemente() {
		int anzahl = zahlenliste.length;
		return anzahl;
	}

	// die Methode soll die Liste ausgeben
	public String toString() {
		String s = Arrays.toString(zahlenliste);
		return s;
	}

	// die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
	// dem Feld vorhanden ist
	public boolean istElement(int zahl) {
		boolean vorhanden = false;
		
		for (int i = 0; i<zahlenliste.length; i++) {
			if (zahlenliste[i] == zahl) {
				vorhanden = true;
				break;
			}
		}
			
		return vorhanden;
	}

	// die Methode soll das erste Vorkommen der
	// als Parameter übergebenen Zahl liefern oder -1 bei nicht vorhanden
	public int getErstePosition(int zahl) {
		int stelle = 0;
		for (int i = 0; i<zahlenliste.length; i++) {
			stelle += 1;
			if (zahlenliste[i] == zahl) {
				break;
			}
			else {
				return -1;
			}
		}
		
		return stelle;
	}

	// die Methode soll die Liste aufsteigend sortieren
	// googlen sie mal nach Array.sort() ;)
	public void sortiere() {
		Arrays.sort(zahlenliste);
	}

	public static void main(String[] args) {
		Felder testenMeinerLösung = new Felder();
		System.out.println(testenMeinerLösung.maxElement());
		System.out.println(testenMeinerLösung.minElement());
		System.out.println(testenMeinerLösung.durchschnitt());
		System.out.println(testenMeinerLösung.anzahlElemente());
		System.out.println(testenMeinerLösung.toString());
		System.out.println(testenMeinerLösung.istElement(9));
		System.out.println(testenMeinerLösung.getErstePosition(5));
		testenMeinerLösung.sortiere();
		System.out.println(testenMeinerLösung.toString());
	}
}
