
public class Buch implements Comparable<Buch>{
	private String autor;
	private String titel;
	private String isbn;
	
	public Buch() {
		super();
	}

	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	
	@Override
	public String toString() {
		return "Buch [autor=" + autor + ", titel=" + titel + ", isbn=" + isbn + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Buch other = (Buch) obj;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (titel == null) {
			if (other.titel != null)
				return false;
		} else if (!titel.equals(other.titel))
			return false;
		return true;
	}

	@Override
	public int compareTo(Buch aBuch) {
		return this.getIsbn().compareTo(aBuch.getIsbn());
	}

	
	
	

	
}
