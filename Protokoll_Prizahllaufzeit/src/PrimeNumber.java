
public class PrimeNumber {
	 long startT, endT;
	 
	 public PrimeNumber() {
		 
	 }
	/*
	 * verifying a number if it's a prime number
	 * @param the potential prime number as a long
	 */
	public boolean isPrime(long number) {
		startT =  System.currentTimeMillis();
		number = Math.abs(number);
		
		if (number < 2) {
			endT = System.currentTimeMillis() - startT;
			System.out.print("Time: "+endT+"ms, false");
			return false;
		}
		
		//going through by all common divisors
		for (long i = 2; i < number; i++) {
		//if is the i a divisor, number != prime number
			if (number % i == 0) {
				endT = System.currentTimeMillis() - startT;
				System.out.print("Time: "+endT+ "ms, false");
				return false;
			}
		}
		//number = prime number
		endT = System.currentTimeMillis() - startT;
		System.out.print("Time: "+endT+"ms, true");
		return true;
	}

}
