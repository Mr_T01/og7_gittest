import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 21.12.2018
  * @author 
  */

public class Hauptfenster extends JFrame {
  // Anfang Attribute
  private JPanel pn_1 = new JPanel(null, true);
    private JLabel l1 = new JLabel();
    private JButton btn_Eintrag = new JButton();
    private JButton btn_Bericht = new JButton();
    private JButton btn_Beenden = new JButton();
  // Ende Attribute
  
  public Hauptfenster() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 872; 
    int frameHeight = 407;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Hauptfenster");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    pn_1.setBounds(0, 0, 852, 372);
    pn_1.setOpaque(false);
    cp.add(pn_1);
    lbl_Ueberschrift.setBounds(8, 8, 427, 65);
    lbl_Ueberschrift.setText("Lerntagebuch von Miriam");
    pn_1.add(lbl_Ueberschrift);
    btn_Eintrag.setBounds(440, 312, 129, 33);
    btn_Eintrag.setText("Neuer Eintrag");
    btn_Eintrag.setMargin(new Insets(2, 2, 2, 2));
    btn_Eintrag.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_Eintrag_ActionPerformed(evt);
      }
    });
    pn_1.add(btn_Eintrag);
    btn_Bericht.setBounds(576, 312, 129, 33);
    btn_Bericht.setText("Bericht");
    btn_Bericht.setMargin(new Insets(2, 2, 2, 2));
    btn_Bericht.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_Bericht_ActionPerformed(evt);
      }
    });
    pn_1.add(btn_Bericht);
    btn_Beenden.setBounds(712, 312, 129, 33);
    btn_Beenden.setText("Beenden");
    btn_Beenden.setMargin(new Insets(2, 2, 2, 2));
    btn_Beenden.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_Beenden_ActionPerformed(evt);
      }
    });
    pn_1.add(btn_Beenden);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Hauptfenster
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Hauptfenster();
  } // end of main
  
  public void btn_Eintrag_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btn_Eintrag_ActionPerformed

  public void btn_Bericht_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btn_Bericht_ActionPerformed

  public void btn_Beenden_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btn_Beenden_ActionPerformed

  // Ende Methoden
} // end of class Hauptfenster

