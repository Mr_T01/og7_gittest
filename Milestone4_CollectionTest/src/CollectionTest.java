import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':	
				System.out.println("Autor: ");
				String autor = myScanner.next();
				
				System.out.println("Tietel: ");
				String tietel = myScanner.next();
				
				System.out.println("ISBN: ");
				String isbn = myScanner.next();
				
				Buch buch = new Buch(autor, tietel, isbn);
                buchliste.add(buch);
				break;
			case '2':
				System.out.println(findeBuch(buchliste, myScanner.next()));
				break;
			case '3':
				System.out.println(loescheBuch(buchliste, myScanner.next()));
				break;
			case '4':
				System.out.println(ermitteleGroessteISBN(buchliste));
				break;		
			case '5':
				System.out.println(buchliste.toString());
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if(buchliste.get(i).getIsbn().equals(isbn)) {
				return buchliste.get(i);
			}
		}
		return null;
	}

	public static boolean loescheBuch (List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if(buchliste.get(i).getIsbn().equals(isbn)) {
				buchliste.remove(i);
				return true;
			}
		}
		return false;
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		String isbn,isbn2,temp;
		if(buchliste.size() != 0 || buchliste.size() != 0) {
			for(int i = 1; i < buchliste.size(); i++) {
				for (int j = 0; j < buchliste.size()-1; j++) {
					isbn = buchliste.get(j).getIsbn();
					isbn2 = buchliste.get(j+1).getIsbn();
					int vergleich = isbn.compareTo(isbn2);
				
					if(vergleich < 0) {													
						temp=buchliste.get(j).getIsbn();
						buchliste.get(j).setIsbn(buchliste.get(j+1).getIsbn());
						buchliste.get(j+1).setIsbn(temp);
					}
				}
			}
		}
		return buchliste.get(0).getIsbn();
	}
}
