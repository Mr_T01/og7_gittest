import java.util.ArrayList;

public class KeyStore02 {
	private ArrayList<String> storage;

	public KeyStore02() {
		this.storage = new ArrayList <String>();
	}

	public int indexOf(String eintrag) {
		return this.storage.indexOf(eintrag);
	}

	public boolean add(String eintrag) {
		return this.storage.add(eintrag);
	}

	public boolean remove(String str) {
		return this.storage.remove(str);
	}

	public boolean remove(int index) {
		this.storage.remove(index);
		return true;
		
	}

	public String get(int index) {

		return this.storage.get(index);
	}

	public int size() {

		return this.storage.size();
	}

	public void clear() {
		for (int i = 0; i < this.storage.size(); i++) {
			this.storage.set(i, null);
		}
	}

	@Override
	public String toString() {
		return "KeyStore02 [storage=" + storage + "]";
	}



}
