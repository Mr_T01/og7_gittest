import java.util.Arrays;

public class KeyStore01 {
	private String[] storage;

	public KeyStore01() {
		this.storage = new String[3];
	}

	public KeyStore01(int length) {
		this.storage = new String[length];
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < this.storage.length; i++) {
			if (eintrag.equals(this.storage[i])) {
				return i;
			}
		}
		return -1;
	}

	public boolean add(String eintrag) {
		for (int i = 0; i < this.storage.length; i++) {
			if (this.storage[i] == null) {
				this.storage[i] = eintrag;
				return true;
			}
		}
		return false;
	}

	public boolean remove(String str) {
		for (int i = 0; i < this.storage.length; i++) {
			if (this.storage[i].equals(str)) {
				this.storage[i] = null;
				return true;
			}
		}
		return false;
		
		
		//return this.remove(this.indexOf(str)); l�sung
	}

	public boolean remove(int index) {

		if (index < storage.length) {
			this.storage[index] = null;
			return true;
		}

		return false;
	}

	public String get(int index) {

		return this.storage[index];
	}

	public int size() {

		return this.storage.length;
	}

	public void clear() {
		for (int i = 0; i < this.storage.length; i++) {
			this.storage[i] = null;
		}
	}

	public String toString() {

		return Arrays.toString(this.storage);
	}

}
