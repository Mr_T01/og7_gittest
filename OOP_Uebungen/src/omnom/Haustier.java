package omnom;

public class Haustier {
	private String name;
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;

	public Haustier (String name) {
		this.name = name;
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
	}

	public String getName() {
		return name;
	}

	
	
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger > 100)
			hunger = 100;
		
		if (hunger < 0)
			hunger = 0;
		
		this.hunger = hunger;
	}

	
	
	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden > 100)
			zufrieden = 100;
		
		if (zufrieden < 0)
			zufrieden = 0;
		
		this.zufrieden = zufrieden;
	}

	
	
	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede > 100)
			muede = 100;
		
		if (muede < 0)
			muede = 0;
		
		this.muede = muede;
	}
	
	

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		
		this.gesund = gesund;
	}
	
	
	public void fuettern(int anzahl) {
		
		this.setHunger(this.getHunger()+anzahl);
	}
	
	public void schlafen(int anzahl) {
		this.setMuede(this.getMuede()+anzahl);
	}
	
	public void spielen(int anzahl) {
		this.setZufrieden(this.getZufrieden()+anzahl);
	}
	
	public void heilen() {
		this.setGesund(100);
	}
}
