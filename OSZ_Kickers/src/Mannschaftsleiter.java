
public class Mannschaftsleiter extends Spieler{
	private String teamName;
	
	public Mannschaftsleiter() {
		// TODO Auto-generated constructor stub
	}
	
	public Mannschaftsleiter(String name, String teleNummer, boolean jahresbeitrag, int trikotNummer, String spielerPosition, String teamName) {
		super(name, teleNummer, jahresbeitrag, trikotNummer, spielerPosition);
		this.teamName = teamName;
	}

	
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

}
