
public class TestKickers {

	public static void main(String[] args) {
//--------------------------------------------------------------------------------------
// Mitglied section
		Mitglied m1 = new Mitglied("Peter", "+49 1345678910", true);
		
		System.out.println("Mitglied m1:"
							+"\n name:"+m1.getName()
							+"\n tele:"+m1.getTeleNummer()
							+"\n jahresbeitrag bezahlt:"+m1.getJahresbeitrag());
		
		
		m1.setName("Olaf");
		m1.setTeleNummer("+49 0198765431");
		m1.setJahresbeitrag(false);
		
		System.out.println("\n Mitglied m1:"
							+"\n name:"+m1.getName()
							+"\n tele:"+m1.getTeleNummer()
							+"\n jahresbeitrag bezahlt:"+m1.getJahresbeitrag());
//--------------------------------------------------------------------------------------
// Trainer section
		
		Trainer t1 = new Trainer("Hans", "+49 66666666", true, 'A');
		
		System.out.println("\n Trainer t1:"
							+"\n name:"+t1.getName()
							+"\n tele:"+t1.getTeleNummer()
							+"\n jahresbeitrag bezahlt:"+t1.getJahresbeitrag()
							+"\n LizensKlasse:"+t1.getLizensKlasse());
		
		t1.setName("Hansi");
		t1.setTeleNummer("+49 777777777");
		t1.setJahresbeitrag(false);
		t1.setLizensKlasse('B');
		
		System.out.println("\n Trainer t1:"
							+"\n name:"+t1.getName()
							+"\n tele:"+t1.getTeleNummer()
							+"\n jahresbeitrag bezahlt:"+t1.getJahresbeitrag()
							+"\n LizensKlasse:"+t1.getLizensKlasse());
//--------------------------------------------------------------------------------------
// Schiedsrichter section
		
		Schiedsrichter s1 = new Schiedsrichter("Fischer", 3);
		
		System.out.println("\n Schiedsrichter s1:"
							+"\n name:"+s1.getName()
							+"\n Anzahlpfiffe:"+s1.getAnzahlPfiffe());
		
		s1.setName("R�stel");
		s1.setAnzahlPfiffe(5);
		
		System.out.println("\n Schiedsrichter s1:"
				+"\n name:"+s1.getName()
				+"\n Anzahlpfiffe:"+s1.getAnzahlPfiffe());
//--------------------------------------------------------------------------------------
// Spieler section
		Spieler sp1 = new Spieler("Tom", "+49 1111111", true, 31, "Sturm");
		
		System.out.println("\n Spieler sp1:"
				+"\n name:"+sp1.getName()
				+"\n tele:"+sp1.getTeleNummer()
				+"\n jahresbeitrag bezahlt:"+sp1.getJahresbeitrag()
				+"\n trikotNummer:"+sp1.getTrikotNummer()
				+"\n position:"+sp1.getSpielerPosition());
		
		sp1.setName("Tim");
		sp1.setTeleNummer("+49 22222222");
		sp1.setJahresbeitrag(false);
		sp1.setTrikotNummer(719);
		sp1.setSpielerPosition("Verteidigung");
		
		System.out.println("\n Spieler sp1:"
				+"\n name:"+sp1.getName()
				+"\n tele:"+sp1.getTeleNummer()
				+"\n jahresbeitrag bezahlt:"+sp1.getJahresbeitrag()
				+"\n trikotNummer:"+sp1.getTrikotNummer()
				+"\n position:"+sp1.getSpielerPosition());
		
//--------------------------------------------------------------------------------------
// Mannschaftsleiter section
		Mannschaftsleiter ml1 = new Mannschaftsleiter("Karl", "+49 000000", true, 25, "Torwart", "Die Karlsberger");
		
		System.out.println("\n Mannschaftsleiter ml1:"
				+"\n name:"+ml1.getName()
				+"\n tele:"+ml1.getTeleNummer()
				+"\n jahresbeitrag bezahlt:"+ml1.getJahresbeitrag()
				+"\n trikotNummer:"+ml1.getTrikotNummer()
				+"\n position:"+ml1.getSpielerPosition()
				+"\n teamname:"+ml1.getTeamName());
		
		ml1.setName("Dennis");
		ml1.setTeleNummer("+49 99999999");
		ml1.setJahresbeitrag(false);
		ml1.setTrikotNummer(189);
		ml1.setSpielerPosition("Mittelfeld");
		ml1.setTeamName("Dennis-Gruppe");
		
		System.out.println("\n Mannschaftsleiter ml1:"
				+"\n name:"+ml1.getName()
				+"\n tele:"+ml1.getTeleNummer()
				+"\n jahresbeitrag bezahlt:"+ml1.getJahresbeitrag()
				+"\n trikotNummer:"+ml1.getTrikotNummer()
				+"\n position:"+ml1.getSpielerPosition()
				+"\n teamname:"+ml1.getTeamName());
		
	}

}
