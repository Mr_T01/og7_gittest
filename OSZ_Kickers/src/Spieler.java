
public class Spieler extends Mitglied {
	private int trikotNummer;
	private String spielerPosition;

	public Spieler() {
		// TODO Auto-generated constructor stub
	}
	
	public Spieler(String name, String teleNummer, boolean jahresbeitrag, int trikotNummer, String spielerPosition) {
		super(name, teleNummer, jahresbeitrag);
		this.trikotNummer = trikotNummer;
		this.spielerPosition = spielerPosition;
	}

	
	public int getTrikotNummer() {
		return trikotNummer;
	}

	public void setTrikotNummer(int trikotNummer) {
		this.trikotNummer = trikotNummer;
	}

	public String getSpielerPosition() {
		return spielerPosition;
	}

	public void setSpielerPosition(String spielerPosition) {
		this.spielerPosition = spielerPosition;
	}

}
