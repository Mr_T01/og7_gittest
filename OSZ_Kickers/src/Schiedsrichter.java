
public class Schiedsrichter {
	private String name;
	private int anzahlPfiffe;

	public Schiedsrichter() {
		// TODO Auto-generated constructor stub
	}
	
	public Schiedsrichter(String name, int anzahlPfiffe) {
		this.name = name;
		this.anzahlPfiffe = anzahlPfiffe;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public int getAnzahlPfiffe() {
		return anzahlPfiffe;
	}

	public void setAnzahlPfiffe(int anzahlPfiffe) {
		this.anzahlPfiffe = anzahlPfiffe;
	}

}
