/**
 * 
 */

/**
 * @author tom-s
 *
 */
public class Mitglied {
	private String name;
	private String teleNummer;
	private boolean jahresbeitrag;

	public Mitglied() {
		
	}

	public Mitglied(String name, String teleNummer, boolean jahresbeitrag) {
		this.name = name;
		this.teleNummer = teleNummer;
		this.jahresbeitrag = jahresbeitrag;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

	public String getTeleNummer() {
		return teleNummer;
	}

	public void setTeleNummer(String teleNummer) {
		this.teleNummer = teleNummer;
	}
	
	
	
	public boolean getJahresbeitrag() {
		return jahresbeitrag;
	}

	public void setJahresbeitrag(boolean jahresbeitrag) {
		this.jahresbeitrag = jahresbeitrag;
	}
	
	
}

