
public class Trainer extends Mitglied{
	private char lizensKlasse;

	public Trainer() {
		// TODO Auto-generated constructor stub
	}
	
	public Trainer(String name, String teleNummer, boolean jahresbeitrag, char lizens) {
		super(name,teleNummer,jahresbeitrag);
		this.lizensKlasse = lizens;
	}
	
	
	public char getLizensKlasse() {
		return lizensKlasse;
	}
	
	public void setLizensKlasse(char lizens) {
		this.lizensKlasse = lizens;
	}

}
