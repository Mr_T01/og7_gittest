public class BinaereSuche {

	public BinaereSuche() {}

	public int binaerSuche (int indexAnfang, int indexEnde, long gesuchteZahl, long[] a) {
		
		int indexMitte = (indexAnfang + indexEnde) / 2;
		
		if (a.length == 0) {
			System.out.println("Leerer Array!");
			return -1;
		}
		
		if (gesuchteZahl == a[indexMitte]) {
			  // System.out.print(a[indexMitte]);
		       return indexMitte;
		       
		   } else if (gesuchteZahl < a[indexMitte]) {
		       return binaerSuche(indexAnfang, indexMitte-1, gesuchteZahl, a);
		        
		   } else {
		       return binaerSuche(indexMitte+1, indexEnde, gesuchteZahl, a);
		   }
	}
}
