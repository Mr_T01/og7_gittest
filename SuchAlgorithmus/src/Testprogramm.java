import java.util.Random;
import java.util.Scanner;

public class Testprogramm {

	/*
	 * LISTEN ERSTELLER
	 */
		public static long[] getSortedList(int laenge){
			Random rand = new Random(111111);
			long[] zahlenliste = new long[laenge];
			long naechsteZahl = 0;
			
			for(int i = 0; i < laenge; i++){
				naechsteZahl += rand.nextInt(3)+1;
				zahlenliste[i] = naechsteZahl;
			} 
			
			return zahlenliste;
		}
	
	public static void main(String[] args) {
		final int ANZAHL = 15000000; //20.000.000
		Scanner input = new Scanner(System.in);
		Random random = new Random(111111);
		
		int zufall = (int )(Math.random() * 20000000 + 0);
		long[] a = getSortedList(ANZAHL);
		long gesuchteZahl = a[zufall];
		
		LineareSuche l1 = new LineareSuche();
		BinaereSuche b1 = new BinaereSuche();
		
		
			long time = System.currentTimeMillis();
			l1.lineareSuche(gesuchteZahl, a);
			System.out.println(System.currentTimeMillis() - time + "ms");
			
			int indexAnfang = 0;
			int indexEnde = a.length-1; 
			long time2 = System.currentTimeMillis();
			b1.binaerSuche(indexAnfang, indexEnde, gesuchteZahl, a);
			System.out.println(System.currentTimeMillis() - time2 + "ms");

			
			}

}
