package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {
	
	private BunteRechteckeController controller;
	
	public Zeichenflaeche (BunteRechteckeController controller) {
		this.controller = controller;
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		//g.drawRect(0, 0, 50, 50);
		for (int i = 0; i < controller.getRechtecke().size(); i++) {
		g.drawRect(controller.getRechtecke().get(i).getX(),//element().getX(),
				   controller.getRechtecke().get(i).getY(),//element().getY(),
				   controller.getRechtecke().get(i).getBreite(),//element().getBreite(),
				   controller.getRechtecke().get(i).getHoehe());//element().getHoehe());
		}
	}
}
