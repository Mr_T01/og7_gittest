package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Eingabemaske extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfd_x;
	private JTextField tfd_y;
	private JTextField tfd_laenge;
	private JTextField tfd_hoehe;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske frame = new Eingabemaske(new BunteRechteckeController());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eingabemaske(BunteRechteckeController brc) {
		this.brc = brc;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnl_eingabemaske = new JPanel();
		contentPane.add(pnl_eingabemaske, BorderLayout.CENTER);
		pnl_eingabemaske.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lbl_x = new JLabel("x");
		pnl_eingabemaske.add(lbl_x);
		
		tfd_x = new JTextField();
		pnl_eingabemaske.add(tfd_x);
		tfd_x.setColumns(10);
		
		JLabel lbl_y = new JLabel("y");
		pnl_eingabemaske.add(lbl_y);
		
		tfd_y = new JTextField();
		pnl_eingabemaske.add(tfd_y);
		tfd_y.setColumns(10);
		
		JLabel lbl_laenge = new JLabel("Laenge");
		pnl_eingabemaske.add(lbl_laenge);
		
		tfd_laenge = new JTextField();
		pnl_eingabemaske.add(tfd_laenge);
		tfd_laenge.setColumns(10);
		
		JLabel lbl_hoehe = new JLabel("Hoehe");
		pnl_eingabemaske.add(lbl_hoehe);
		
		tfd_hoehe = new JTextField();
		pnl_eingabemaske.add(tfd_hoehe);
		tfd_hoehe.setColumns(10);
		
		JButton btn_speichern = new JButton("speichern");
		pnl_eingabemaske.add(btn_speichern);
		btn_speichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btn_SpeichernClicked();
			}
		});
		setVisible(true);
	}

	protected void btn_SpeichernClicked() {
		int x = Integer.parseInt(tfd_x.getText());
		int y = Integer.parseInt(tfd_y.getText());
		int laenge = Integer.parseInt(tfd_laenge.getText());
		int hoehe = Integer.parseInt(tfd_hoehe.getText());
		Rechteck r = new Rechteck(x,y, laenge, hoehe);
		brc.add(r);
	}

}
