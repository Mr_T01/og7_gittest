package model;
import java.util.concurrent.ThreadLocalRandom;

public class Rechteck {

	private Punkt p;
	private int breite;
	private int hoehe;

	public Rechteck() {
		this.p = new Punkt(0,0);
		this.breite = 0;
		this.hoehe = 0;
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		
		this.p = new Punkt(x,y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public boolean enthaelt(Punkt p) {
		boolean b = false;
		Punkt rightCornerPoint = new Punkt(this.p.getX()+breite, this.p.getY()+hoehe);
		
		if(p.getX() >= this.p.getX() && p.getX() <= rightCornerPoint.getX() && p.getY() >= this.p.getY() && p.getY() <= rightCornerPoint.getY())
		b = true;
		
		return b;
	}
	
	public boolean enthaelt(Rechteck r) {
		boolean b = false;
		Punkt rectangleUppperLeftCorner = new Punkt(r.getX(), r.getY());
		Punkt rectangleRightCorner = new Punkt(r.getX()+r.getBreite(), r.getY()+r.getHoehe());
		
		if(this.enthaelt(rectangleUppperLeftCorner)
		&& this.enthaelt(rectangleRightCorner)) return b = true;
		
		return b;
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Rechteck r;
		int randomX;
		int randomY;
		int randomBreite;
		int randomHoehe;
		boolean b = true;
		
		do {
			randomX = ThreadLocalRandom.current().nextInt(0, 1200 + 1);
			randomY = ThreadLocalRandom.current().nextInt(0, 1000 + 1);
			randomBreite = ThreadLocalRandom.current().nextInt(0, 1200 + 1);
			randomHoehe = ThreadLocalRandom.current().nextInt(0, 1000 + 1);
			
			r = new Rechteck(randomX, randomY, randomBreite, randomHoehe);
			
			if(randomX+randomBreite <= 1200 && randomY+randomHoehe <=1000) {
				b = false;
			}
		}
		while(b);
		
		return r;
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

}
