package test;

import model.Rechteck;

public class RechteckTest {
	public static void main (String[] args) {
		Rechteck rechteck0 = new Rechteck();
		Rechteck rechteck1 = new Rechteck();
		Rechteck rechteck2 = new Rechteck();
		Rechteck rechteck3 = new Rechteck();
		Rechteck rechteck4 = new Rechteck();
		Rechteck rechteck5 = new Rechteck(100,100,100,100);
		Rechteck rechteck6 = new Rechteck(200,200,200,200);
		Rechteck rechteck7 = new Rechteck(800,400,20,20);
		Rechteck rechteck8 = new Rechteck(800,450,20,20);
		Rechteck rechteck9 = new Rechteck(850,400,20,20);
	}
	
	public static boolean rechteckeTesten() {
		Rechteck r[] = new Rechteck [50000];
		Rechteck pR = new Rechteck(0, 0, 1200, 1000);
		boolean b = false;
		
		for (int i = 0; i <= 50000; i++) {
			r[i] = new Rechteck().generiereZufallsRechteck();
			
			if(pR.enthaelt(r[i])) return b = true;
		}
		return b;
	}
}
