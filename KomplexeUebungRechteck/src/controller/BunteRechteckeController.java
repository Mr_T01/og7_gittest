package controller;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.Eingabemaske;

public class BunteRechteckeController {
	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public BunteRechteckeController () {
		//rechtecke = new LinkedList<Rechteck>();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}
	public static void main(String[] args) {
		
	}
	
	public void add(Rechteck rechteck) {
		this.database.rechteckEintragen(rechteck);
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		this.rechtecke.clear();
	}
	
	public List<Rechteck> getRechtecke() {
		return this.rechtecke;
	}

	public void generiereZufallsRechtecke(int anzahl) {
		reset();
		for (int i = 0; i <= anzahl; i++) {
		Rechteck r = new Rechteck().generiereZufallsRechteck();
		add(r);
		}
	}
	
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + this.rechtecke + "]";
	}

	public void rechteckHinzufügen() {
		new Eingabemaske(this);
		
	}
}
